# AAAA | Nom de la structure bénéficiaire x Latitudes

_Ce repository est un exemple pour illustrer l'arborescence à utiliser et la documentation à mettre en place. Vous avez également à votre disposition un repo exemple pour comprendre comment mettre en application ces conseils : [1617_voxe](https://gitlab.com/latitudes-exploring-tech-for-good/voxe/1617_voxe)._

Chaque document présent dans ce dossier sera nommé "AAMMJJ - Nom du document" pour faciliter la compréhension.
Si nécessaire, précisisez ici les grands objectifs de chaque jalon/livrable intermédiaire.

## Contexte
* Rappelez le nom de l'établissement, votre année d'étude, le cursus dans lequel vous êtes.
* Renseignez le temps qui vous est alloué, la fréquence des séances de travail, le nombre d'étudiant.e.s dans votre groupe.
* Explicitez la manière dont le projet est suivi par l'établissement (réunions, soutenances, rapports, jalons interemédiaire). Il n'est pas nécessaire de rentrer dans le détail de chaque livrable intermédiaire mais plutôt expliciter la démarche gloabale de l'établissement et de votre créneau projet.
* Gardez en tête que ces explication seront utiles aux Tech for Good Enthusiasts qui souhaiteraient reprendre le projet après vous.

Vous trouverez plus de détails sur le contexte et les perspectives d'évolutions du projet dans le dossier parent.
L'accès au code source se fait via le dossier ["code"](../code/).
