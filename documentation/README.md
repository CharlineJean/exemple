# AAAA | Nom de la structure bénéficiaire x Latitudes

_Ce repository est un exemple pour illustrer l'arborescence à utiliser et la documentation à mettre en place. Vous avez également à votre disposition un repo exemple pour comprendre comment mettre en application ces conseils : [1617_voxe](https://gitlab.com/latitudes-exploring-tech-for-good/voxe/1617_voxe)._

Cette partie contient la documentation détaillée du projet AAAA_nom-de-la-structure-bénéficiaire, avec notamment les livrables :
* [relatifs à "Nom de l'établissement scolaire"](./nom-de-l'établissement-scolaire/).
* faisant partie intégrante de [l'accompagnement Lattitudes](./latitudes/).

Vous trouverez plus de détails sur le contexte et les perspectives d'évolutions du projet dans le dossier parent.
L'accès au code source se fait via le dossier ["code"](../code/).
