# AAAA | Nom de la structure bénéficiaire x Latitudes

_Ce repository est un exemple pour illustrer l'arborescence à utiliser et la documentation à mettre en place. Vous avez également à votre disposition un repo exemple pour comprendre comment mettre en application ces conseils : [1617_voxe](https://gitlab.com/latitudes-exploring-tech-for-good/voxe/1617_voxe)._

Cette partie contient le produit développé dans le cadre du projet AAAA_nom-de-la-structure-bénéficiaire.

## Architecture globale
* Présentez ici la manière dont votre code est architecturé.
* Pensez à expliquer à quoi servent vos différents fichiers, si nécessaire les variables que vous utilisés, des composants particuliers, etc.

## Quickstart
* Cette partie est utile pour toute personne qui souhaite reprendre le projet : que doit-elle faire, quelles dépendances faut-il qu'elle installe ? Présentez cela étape par étape en renseignant les démarches nécessaires.

Vous trouverez plus de détails sur le contexte et les perspectives d'évolutions du projet dans le dossier parent.
L'accès à la documentation détaillée se fait dans le dossier ["documentation"](../documentation/).
